/*
 * Simple array of bits (bit map) C library
 * Copyright (C) 2020  David Moreno
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <stdlib.h>
#include <stdint.h>
#include <limits.h>
#include <string.h>
#include "bitarray.h"

static int _bita_error_ = BITA_NO_ERROR;

typedef struct bita_t {
	uint8_t *map; /* TODO: It could be more efficient using machine's word size instead of bytes? */
	unsigned int bytes;  /* Number of bytes. */
	unsigned long bits;  /* Number of bits. */
	unsigned long limit; /* Maximum usable bits. */
} bita_t;

bita_t *bita_new (unsigned long bitnum)
{
	bita_t *new;
	unsigned int elem;

	if (bitnum < 1) {
		_bita_error_ = BITA_ERROR_INSUFFICIENT;
		return NULL;
	}

	new = calloc(1, sizeof(bita_t));
	if (new == NULL) {
		_bita_error_ = BITA_ERROR_MEM;
		return NULL;
	}

	if (bitnum <= CHAR_BIT) {
		elem = 1;
	} else {
		elem = bitnum / CHAR_BIT;
		if (bitnum % CHAR_BIT) elem++;
	}
	
	new->map = calloc(elem, sizeof(uint8_t));
	if (new->map == NULL) {
		free(new);
		_bita_error_ = BITA_ERROR_MEM;
		return NULL;
	}

	new->bytes = elem;
	new->bits = elem * CHAR_BIT;
	new->limit = bitnum - 1;

	return new;
}

void bita_free (bita_t *obj)
{
	if (obj->map != NULL) free(obj->map);
	free(obj);
}

int bita_set (bita_t *obj, unsigned long pos)
{
	int i=0, r=0;
	uint8_t mask=1;

	if (pos > obj->limit) {
		_bita_error_ = BITA_ERROR_OUT_UPPER_BOUND;
		return -1;
	}

	i = pos / CHAR_BIT;
	r = pos % CHAR_BIT;

	mask <<= r;
	obj->map[i] |= mask;

	return pos;
}

int bita_unset (bita_t *obj, unsigned long pos)
{
	int i=0, r=0;
	uint8_t mask=1;

	if (pos > obj->limit) {
		_bita_error_ = BITA_ERROR_OUT_UPPER_BOUND;
		return -1;
	}

	i = pos / CHAR_BIT;
	r = pos % CHAR_BIT;

	mask <<= r;
	mask = ~mask;

	obj->map[i] &= mask;

	return pos;
}

int bita_get (bita_t *obj, unsigned long pos)
{
	int i=0, r=0;
	uint8_t mask=1;

	if (pos > obj->limit) {
		_bita_error_ = BITA_ERROR_OUT_UPPER_BOUND;
		return -1;
	}

	i = pos / CHAR_BIT;
	r = pos % CHAR_BIT;

	mask <<= r;

	return (obj->map[i] & mask) ? 1: 0;
}

unsigned long bita_get_min (bita_t *obj)
{
	unsigned long byte, bit;
	uint8_t found=0, mask=1;

	for (byte=0; byte < obj->bytes; byte++)
		if (obj->map[byte] != 0) {
			found=1;
			break;
		}

	if (!found) return 0;

	for (bit=0; bit < CHAR_BIT; bit++) {
		if (obj->map[byte] & mask) break;
		mask <<= 1;
	}

	return (byte * CHAR_BIT) + bit;
}

unsigned long bita_get_max (bita_t *obj)
{
	unsigned long byte, bit;
	uint8_t found=0, mask=1;

	for (byte=obj->bytes; byte > 0; byte--) {
		if (obj->map[byte-1] != 0) {
			found=1;
			break;
		}
	}

	if (!found) return 0;
	mask <<= (CHAR_BIT - 1);

	for (bit=CHAR_BIT - 1; bit >= 0; bit--) {
		if (obj->map[byte-1] & mask) break;
		mask >>= 1;
	}

	return (byte * CHAR_BIT) - (CHAR_BIT - bit);
}

void bita_set_all (bita_t *obj)
{
	memset(obj->map, ~0, obj->bytes);
}

void bita_unset_all (bita_t *obj)
{
	memset(obj->map, 0, obj->bytes);
}

unsigned long bita_get_next (bita_t *obj, unsigned long pos)
{
	unsigned int r, start;
	unsigned long byte, bit;
	uint8_t mask=1;

	if (pos >= obj->limit) {
		_bita_error_ = BITA_ERROR_OUT_UPPER_BOUND;
		return bita_get_max(obj);
	}

	if ((++pos) >= obj->limit) {
		_bita_error_ = BITA_ERROR_OUT_UPPER_BOUND;
		return bita_get_max(obj);
	}

	byte = pos / CHAR_BIT;
	r = pos % CHAR_BIT;
	mask <<= r;
	start = r;

	while (byte < obj->bytes) {
		for (bit=start; bit < CHAR_BIT; bit++) {
			if (obj->map[byte] & mask) return (byte * CHAR_BIT) + bit;
			mask <<= 1;
		}
		byte++;
		start = 0;
		mask = 1;
	}

	return --pos;
}

unsigned int bita_get_bytenum (bita_t *obj)
{
	return obj->bytes;
}

unsigned long bita_get_bitnum (bita_t *obj)
{
	return obj->bits;
}

unsigned long bita_get_limit (bita_t *obj)
{
	return obj->limit;
}

int bita_get_error (void)
{
	return _bita_error_;
}
