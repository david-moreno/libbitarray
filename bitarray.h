/*
 * This file is part of libbitarray.
 * 
 * libbitarray is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * libbitarray is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with libbitarray.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef BITARRAY_H
#define BITARRAY_H

enum {
	BITA_NO_ERROR,
	BITA_ERROR_INSUFFICIENT,
	BITA_ERROR_MEM,
	BITA_ERROR_OUT_UPPER_BOUND
};

typedef struct bita_t bita_t;

bita_t *bita_new (unsigned long bitnum);
void bita_free (bita_t *obj);
int bita_set (bita_t *obj, unsigned long pos);
int bita_unset (bita_t *obj, unsigned long pos);
int bita_get (bita_t *obj, unsigned long pos);
unsigned long bita_get_min (bita_t *obj);
unsigned long bita_get_max (bita_t *obj);
void bita_set_all (bita_t *obj);
void bita_unset_all (bita_t *obj);
unsigned long bita_get_next (bita_t *obj, unsigned long pos);
unsigned int bita_get_bytenum (bita_t *obj);
unsigned long bita_get_bitnum (bita_t *obj);
unsigned long bita_get_limit (bita_t *obj);
int bita_get_error (void);

#endif /* End of BITARRAY_H */
