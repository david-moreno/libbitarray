NAME=libbitarray
VERSION=1.1.6
INC=bitarray.h
OBJ=bitarray.o
SO=$(NAME).so
LIB=$(NAME).so.$(VERSION)
LIBDIR=/usr/lib
INCDIR=/usr/include
LIBSYM=$(LIBDIR)/libbitarray.so
CC=gcc

$(LIB): bitarray.c bitarray.h
	$(CC) -c -Wall -Werror -fpic $<
	$(CC) -shared -o $@ $(OBJ)

install:
	install -m644 -s $(LIB) $(LIBDIR)
	ln -s $(LIBDIR)/$(LIB) $(LIBSYM)
	install -m644 $(INC) $(INCDIR)

uninstall remove:
	rm -fv $(LIBSYM)
	rm -fv $(LIBDIR)/$(LIB)
	rm -fv $(INCDIR)/$(INC)

clean:
	rm -fv $(OBJ) $(LIB)
